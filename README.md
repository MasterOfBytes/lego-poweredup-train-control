# Lego PoweredUp Train Control

Simple Console App to control a PoweredUp Lego Train.  

At the current Stage the App is not really user friendly. For Example it does not check for any errors in the XML file, it will just shutdown on errors. Also everything is very quick & dirty.
It is just an app for me to control the train when i play with my child. But maybe it helps someone.  

## Usage

Change Hotkeys as you like in the Hotkeys.xml.  
Then start the App and press the connection button on the train. After some seconds it should connect. 
When you see ```Ready. Start pressing your Hotkeys!``` in the Console it is connected and you can start using the hotkeys. Also the LED on the Train should turned white.

## Hotkey configuration

```xml
	<Key consoleKey="D1" ctrl="false" shift="false" alt="false">
		<SetSpeed speed="60" />
	</Key>
```

### Key

```consoleKey``` is parsed as ConsoleKey. You can find all possible values at <https://learn.microsoft.com/de-de/dotnet/api/system.consolekey?view=net-5.0>.  
```ctrl```, ```shift``` and ```alt``` are optional, default is false.

### Actions

You can place as many Actions in a HotKey as you want. Possible Actions are:

* ```<SetSpeed speed="50" />``` Setting the Motor Speed to ```speed``` value. ( See below )
* ```<Break />```: Stops the train by brake. Setting Speed to 0 will break slower.
* ```<SetLED r="0" g="0" b="0" />```: Sets the color of the Hub-Led.
* ```<SetLEDBlinking r="255" g="0" b="0" r2="0" g2="0" b2="0" delayBetweenBlinks="100" />```: Starts to blink the Hub-Led between rgb & r2g2b2 with a Delay of ```delayBetweenBlinks``` Miliseconds. rgb2 is optional and default to black. Delay is optinal and default to 100.
* ```<Sleep timeMS="300" />```: Waits ```timeMS``` miliseconds until the next action.
* ```<Quit />```: Stops the train, disconnects and quits the App.
* ```<MotorInfo />```: Displays Motorinfo in the Console. ( Seems to not work so far. )

Be aware, that the speed-Value is not percentage or something. I acutally could not figure out, how the speed is calculated for that value. The documentation says:
> Power levels in percentage: 1 - 100 (CW), -1 - -100 (CCW)  

SetLEDBlinking is sometimes a bit buggy. When it doesn't start blinking, just press the key again. Seems to be an issue with the async task stuff, but i don't want to go futher with that now. 

## Planned features

* [ ]: Make it configurable at which port the Motor is.
* [ ]: Saving the current Speed intern, so that it will be possible to add a slow acceleration out of the box. ( So not doing SetSpeed 10, SetSpeed 20, SetSpeed 30 but ```<SetSpeed speed="100" slowAccelerationBy="10" accelerationWaitBetween="50" />```)
* [ ]: Getting Battery Status from the HUB.