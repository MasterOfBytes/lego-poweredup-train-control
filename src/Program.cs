﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SharpBrick.PoweredUp;
using SharpBrick.PoweredUp.WinRT;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Linq;
using System.Reflection;

namespace LegoTrain
{
	class Program
	{
		static List<HotKey> hotKeys = new List<HotKey>();

		static async Task Main(string[] args)
		{
			var serviceProvider = new ServiceCollection()
				 .AddLogging()
				 .AddPoweredUp()
				 .AddWinRTBluetooth() // using WinRT Bluetooth on Windows (separate NuGet SharpBrick.PoweredUp.WinRT; others are available)
				 .BuildServiceProvider();

			loadHotKeys();
			if (hotKeys.Count <= 0)
			{
				Console.WriteLine("Did not load any Hotkey, exiting...");
				return;
			}

			Console.Write("GetService...");
			var host = serviceProvider.GetService<PoweredUpHost>();
			Console.WriteLine("done");

			Console.Write("Discover...");
			var hub = await host.DiscoverAsync<TwoPortHub>();
			Console.WriteLine("done");

			//host.Discover(async hub =>
			//{
			//	Console.WriteLine("Connecting to Hub");
			//	await hub.ConnectAsync();

			//	Console.WriteLine(hub.AdvertisingName);
			//	Console.WriteLine(hub.SystemType.ToString());

			//});

			Console.WriteLine("Hubinfo:");
			Console.WriteLine(hub.AdvertisingName);
			Console.WriteLine(hub.SystemType.ToString());


			Console.Write("Connecting...");
			await hub.ConnectAsync();
			Console.WriteLine("done");

			Console.WriteLine("Ports:");
			foreach (var port in hub.Ports)
			{
				Console.WriteLine($"Port {port.PortId}: IsDeviceAttached: {port.IsDeviceAttached}; FriendlyName: {port.FriendlyName}; DeviceType: {port.DeviceType}");
			}

			var motor = hub.Port(0).GetDevice<SystemTrainMotor>();
			var light = hub.Port(50).GetDevice<RgbLight>();
			var current = hub.Port(59).GetDevice<Current>();
			var voltage = hub.Port(60).GetDevice<Voltage>();

			Console.WriteLine($"Motor? {motor}");
			Console.WriteLine($"light? {light}");
			Console.WriteLine($"current? {current}");
			Console.WriteLine($"voltage? {voltage}");

			await light.SetRgbColorsAsync(255, 255, 255);

			var neededDevices = new List<Device>() { motor, new MotorLED(light) };

			Console.WriteLine();
			Console.WriteLine($"Ready. Start pressing your Hotkeys!");
			var doLoop = true;
			while (doLoop)
			{
				try
				{
					var input = Console.ReadKey(false);
					var hotkey = findHotKey(input);
					if (hotkey != null)
					{
						Console.WriteLine($"Found Hotkey {hotkey}");
						foreach (var action in hotkey.HotKeyActions)
						{
							if (action.GetType().Name == nameof(Quit))
							{
								doLoop = false;
								break;
							}
							await action.DoActionAsync(neededDevices);
						}
					}
				}
				catch (Exception ex)
				{
					Console.WriteLine($"Error {ex.Message}");
				}
			}

			Console.Write("Stopping...");
			await motor.StopByBrakeAsync();
			Console.WriteLine("done");

			Console.Write("Switching off...");
			await hub.SwitchOffAsync();
			Console.WriteLine("done");
		}

		static void loadHotKeys()
		{
			Console.WriteLine("Loading Keys...");
			
			var fileName = ".\\Hotkeys.xml";
			var xRoot = XDocument.Load(fileName).Root;
			foreach (var xmlKey in xRoot.Elements("Key"))
			{
				var ctrl = false;
				var alt = false;
				var shift = false;

				var keyString = xmlKey.Attribute("consoleKey").Value;
				if (Enum.TryParse<ConsoleKey>(keyString, true, out var consoleKey))
				{
					if (xmlKey.TryGetAttributeValue("ctrl", out var ctrlVal))
					{
						ctrl = bool.Parse(ctrlVal);
					}
					if (xmlKey.TryGetAttributeValue("shift", out var shiftVal))
					{
						shift = bool.Parse(shiftVal);
					}
					if (xmlKey.TryGetAttributeValue("alt", out var altVal))
					{
						alt = bool.Parse(altVal);
					}

					var hotKey = new HotKey(new ConsoleKeyInfo(keyString[0], consoleKey, shift, alt, ctrl)) { XMLString = xmlKey.ToString() };

					foreach (var xmlAction in xmlKey.Elements()) 
					{
						var actionName = xmlAction.Name.LocalName;
						var actionClassType = Assembly.GetExecutingAssembly().GetTypes().FirstOrDefault(t => t.GetInterface(nameof(HotKeyAction)) != null && t.Name.ToUpper() == actionName.ToUpper());
						var actionInstance = Activator.CreateInstance(actionClassType) as HotKeyAction;
						actionInstance.Load(xmlAction);
						hotKey.HotKeyActions.Add(actionInstance);
					}
					
					hotKeys.Add(hotKey);
					Console.WriteLine("   Added HotKey " + hotKey);
				}
				else
				{
					Console.WriteLine($"   Could not parse Key {keyString}");
				}
			}
	
			Console.WriteLine($"   done. Loaded {hotKeys.Count} Keys");
		}

		static HotKey findHotKey(ConsoleKeyInfo consoleKeyInfo)
		{
			var foundKeys = hotKeys.Where(k => k.ConsoleKeyInfo.Key == consoleKeyInfo.Key);

			if (consoleKeyInfo.Modifiers.HasFlag(ConsoleModifiers.Alt))
			{
				foundKeys = foundKeys.Where(k => k.ConsoleKeyInfo.Modifiers.HasFlag(ConsoleModifiers.Alt));
			}
			if (consoleKeyInfo.Modifiers.HasFlag(ConsoleModifiers.Shift))
			{
				foundKeys = foundKeys.Where(k => k.ConsoleKeyInfo.Modifiers.HasFlag(ConsoleModifiers.Shift));
			}
			if (consoleKeyInfo.Modifiers.HasFlag(ConsoleModifiers.Control))
			{
				foundKeys = foundKeys.Where(k => k.ConsoleKeyInfo.Modifiers.HasFlag(ConsoleModifiers.Control));
			}

			return foundKeys.FirstOrDefault();
		}
	}
}
