﻿using SharpBrick.PoweredUp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LegoTrain
{
	class MotorLED : Device
	{
		private readonly RgbLight rgbLight;
		private Blinker blinker;

		public MotorLED(RgbLight rgbLight)
		{
			this.rgbLight = rgbLight;
		}

		public async Task SetColorAsync(byte r, byte g, byte b)
		{
			stopBlinking();
		
			await rgbLight.SetRgbColorsAsync(r, g, b);
		}

		public void SetBlinking(byte r1, byte g1, byte b1, byte r2, byte g2, byte b2, int delayBetweenBlinksInMS)
		{
			stopBlinking();

			blinker = new Blinker(new BlinkOptions(r1, g1, b1, r2, g2, b2, delayBetweenBlinksInMS, rgbLight)).Start();
		}

		private void stopBlinking()
		{
			blinker?.Stop();
		}


		class Blinker
		{
			private readonly BlinkOptions blinkOptions;
			private readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
			private Task blinkTask;

			public Blinker(BlinkOptions blinkOptions)
			{
				this.blinkOptions = blinkOptions;
			}

			public Blinker Start()
			{
				blinkTask = Task.Run(async () => {await blink(); }, cancellationTokenSource.Token);
				return this;
			}

			public void Stop()
			{
				Console.WriteLine("Stopping blinker");
				cancellationTokenSource?.Cancel();
				if (blinkTask != null && blinkTask.Status != TaskStatus.Canceled)
				{
					Console.Write("Waiting for Blinker to finish...");
					blinkTask?.Wait();
					Console.WriteLine("done waiting for Blinker to finish");
				}
				Console.WriteLine("done stopping blinker");
			}

			private async Task blink()
			{
				while (!cancellationTokenSource.Token.IsCancellationRequested)
				{
					Console.Write($"Setting LED to {blinkOptions.R1},{blinkOptions.G1},{blinkOptions.B1}...");
					await blinkOptions.RgbLight.SetRgbColorsAsync(blinkOptions.R1, blinkOptions.G1, blinkOptions.B1);
					Console.WriteLine("done");

					if (cancellationTokenSource.Token.IsCancellationRequested)
					{
						Console.WriteLine("Cancelling");
						return;
					}

					Console.Write($"Waiting {blinkOptions.DelayBetweenBlinksMS} MS...");
					await Task.Delay(blinkOptions.DelayBetweenBlinksMS, cancellationTokenSource.Token);
					Console.WriteLine("done");

					if (cancellationTokenSource.Token.IsCancellationRequested)
					{
						Console.WriteLine("Cancelling");
						return;
					}
					Console.Write($"Setting LED to {blinkOptions.R2},{blinkOptions.G2},{blinkOptions.B2}...");
					await blinkOptions.RgbLight.SetRgbColorsAsync(blinkOptions.R2, blinkOptions.G2, blinkOptions.B2);
					Console.WriteLine("done");

					if (cancellationTokenSource.Token.IsCancellationRequested)
					{
						Console.WriteLine("Cancelling");
						return;
					}

					Console.Write($"Waiting {blinkOptions.DelayBetweenBlinksMS} MS...");
					await Task.Delay(blinkOptions.DelayBetweenBlinksMS, cancellationTokenSource.Token);
					Console.WriteLine("done");
				}
			}
		}

		class BlinkOptions
		{
			public byte R1 { get; }
			public byte G1 { get; }
			public byte B1 { get; }
			public byte R2 { get; }
			public byte G2 { get; }
			public byte B2 { get; }
			public int DelayBetweenBlinksMS { get; }
			public RgbLight RgbLight { get; }

			public BlinkOptions(byte r1, byte g1, byte b1, byte r2, byte g2, byte b2, int delayBetweenBlinksMS, RgbLight rgbLight)
			{
				R1 = r1;
				G1 = g1;
				B1 = b1;
				R2 = r2;
				G2 = g2;
				B2 = b2;
				DelayBetweenBlinksMS = delayBetweenBlinksMS;
				RgbLight = rgbLight;
			}
		}
	}
}
