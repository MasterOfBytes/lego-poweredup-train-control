﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LegoTrain
{
	public static class XMLFunctions
	{
		public static bool TryGetElement(this XElement xElement, string name, out XElement value)
		{
			var subElement = xElement.Element(name);
			if (subElement != null)
			{
				value = subElement;
				return true;
			}

			value = default;
			return false;
		}

		public static bool TryGetAttributeValue(this XElement xElement, string name, out string value)
		{
			var attr = xElement.Attribute(name);
			if (attr != null)
			{
				value = attr.Value;
				return true;
			}

			value = default;
			return false;
		}
	}

}
