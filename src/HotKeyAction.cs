﻿using SharpBrick.PoweredUp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LegoTrain
{
	interface HotKeyAction
	{
		void Load(XElement xElement);
		Task DoActionAsync(List<Device> devices);
	}

	class SetSpeed : HotKeyAction
	{
		public sbyte Speed { get; private set; }

		public void Load(XElement xElement)
		{
			Speed = sbyte.Parse(xElement.Attribute("speed").Value);
		}
		/*
			 /// <summary>
			 /// Starts the motor with full speed at the given power level.
			 /// </summary>
			 /// <param name="power">
			 /// - Power levels in percentage: 1 - 100 (CW), -1 - -100 (CCW)
			 /// - Stop Motor (floating): 0 
			 /// - Stop Motor (breaking): 127
			 /// </param>
			 /// <returns>An awaitable Task.</returns>


		 */
		public async Task DoActionAsync(List<Device> devices)
		{
			Console.WriteLine($"Setting Motor to {Speed}");
			await devices.OfType<SystemTrainMotor>().FirstOrDefault()?.StartPowerAsync(Speed);
		}

		public override string ToString() => $"SetSpeed {Speed};";
	}

	class SetLED : HotKeyAction
	{
		public byte R { get; private set; }
		public byte G { get; private set; }
		public byte B { get; private set; }

		public void Load(XElement xElement)
		{
			R = byte.Parse(xElement.Attribute("r").Value);
			G = byte.Parse(xElement.Attribute("g").Value);
			B = byte.Parse(xElement.Attribute("b").Value);
		}

		public async Task DoActionAsync(List<Device> devices)
		{
			Console.WriteLine($"Setting LED to {R},{G},{B}");
			await devices.OfType<MotorLED>().FirstOrDefault()?.SetColorAsync(R, G, B);
		}

		public override string ToString() => $"SetLED {R},{G},{B}";
	}

	class SetLEDBlinking : HotKeyAction
	{
		public byte R1 { get; private set; }
		public byte G1 { get; private set; }
		public byte B1 { get; private set; }
		public byte R2 { get; private set; } = 0;
		public byte G2 { get; private set; } = 0;
		public byte B2 { get; private set; } = 0;
		public int DelayBetweenBlinksMS { get; private set; } = 100;

		public void Load(XElement xElement)
		{
			R1 = byte.Parse(xElement.Attribute("r").Value);
			G1 = byte.Parse(xElement.Attribute("g").Value);
			B1 = byte.Parse(xElement.Attribute("b").Value);

			if (xElement.TryGetAttributeValue("r2", out var r2))
			{
				R2 = byte.Parse(r2);
			}
			if (xElement.TryGetAttributeValue("g2", out var g2))
			{
				G2 = byte.Parse(g2);
			}
			if (xElement.TryGetAttributeValue("b2", out var b2))
			{
				B2 = byte.Parse(b2);
			}

			if (xElement.TryGetAttributeValue("delayBetweenBlinks", out var delay))
			{
				DelayBetweenBlinksMS = int.Parse(delay);
			}
		}

		public async Task DoActionAsync(List<Device> devices)
		{
			Console.WriteLine($"Setting LED to blink between {R1},{G1},{B1} and {R2},{G2},{B2}   {DelayBetweenBlinksMS} Delay");
			await Task.Run(()=> { devices.OfType<MotorLED>().FirstOrDefault()?.SetBlinking(R1, G1, B1, R2, G2, B2, DelayBetweenBlinksMS); });
		}

		public override string ToString() => $"SetLEDBlinking {R1},{G1},{B1} and {R2},{G2},{B2}   {DelayBetweenBlinksMS} Delay";
	}


	class Break : HotKeyAction
	{
		public void Load(XElement xElement)
		{
		}

		public async Task DoActionAsync(List<Device> devices)
		{
			Console.WriteLine($"Stopping by Break");
			await devices.OfType<SystemTrainMotor>().FirstOrDefault()?.StopByBrakeAsync();
		}

		public override string ToString() => $"Break";
	}

	class Sleep : HotKeyAction
	{
		public int TimeInMS { get; private set; }

		public void Load(XElement xElement)
		{
			TimeInMS = int.Parse(xElement.Attribute("timeMS").Value);
		}

		public async Task DoActionAsync(List<Device> devices)
		{
			Console.WriteLine($"Delaying {TimeInMS} ms");
			await Task.Delay(TimeInMS);
		}

		public override string ToString() => $"Sleep {TimeInMS} ms;";
	}

	class Quit : HotKeyAction
	{
		public void Load(XElement xElement)
		{
		}

#pragma warning disable CS1998 // Bei der asynchronen Methode fehlen "await"-Operatoren. Die Methode wird synchron ausgeführt.
		public async Task DoActionAsync(List<Device> devices)
#pragma warning restore CS1998 // Bei der asynchronen Methode fehlen "await"-Operatoren. Die Methode wird synchron ausgeführt.
		{
			Console.WriteLine($"Quitting");
		}

		public override string ToString() => $"Quit";
	}

	class MotorInfo : HotKeyAction
	{
		public void Load(XElement xElement)
		{
		}
#pragma warning disable CS1998 // Bei der asynchronen Methode fehlen "await"-Operatoren. Die Methode wird synchron ausgeführt.
		public async Task DoActionAsync(List<Device> devices)
#pragma warning restore CS1998 // Bei der asynchronen Methode fehlen "await"-Operatoren. Die Methode wird synchron ausgeführt.
		{
			var motor = devices.OfType<SystemTrainMotor>().FirstOrDefault();
			if (motor != null)
			{
				Console.WriteLine($"Motorinfo: Power: {motor.Power}; PowerPct: {motor.PowerPct}; ModeIndexPower: {motor.ModeIndexPower}; Modes: {string.Join(',', motor.Modes)};");
			}
		}

		public override string ToString() => $"MotorInfo";
	}
}
