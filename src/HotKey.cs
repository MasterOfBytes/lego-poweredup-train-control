﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegoTrain
{
	class HotKey
	{
		public ConsoleKeyInfo ConsoleKeyInfo { get; }

		public List<HotKeyAction> HotKeyActions { get; set; } = new List<HotKeyAction>();
		public string XMLString { get; set; }

		public HotKey(ConsoleKeyInfo consoleKeyInfo)
		{
			ConsoleKeyInfo = consoleKeyInfo;
		}

		public override string ToString() => $"{ConsoleKeyInfo.Key}; {string.Join(',', HotKeyActions)}            {XMLString.Replace("\r\n", "  ")}";
	}
}
